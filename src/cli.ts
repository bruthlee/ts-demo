import { program } from 'commander'
import HttpServer, { IHttpServerOptions } from './http-serve'

program
    .option('--cache, <cache>', '设置缓存时间，秒数')
    .option('--root, <root>', '静态文件目录')
    .option('-p, --p, <port>', '监听端口', '3000')
    .action((options: Omit<IHttpServerOptions, 'cache'> & { cache?: string; port: string }) => {
        const {root, cache, port} = options
        const cache_number: number = cache ? parseInt(cache) : 3600
        const server = new HttpServer({
            root, 
            cache: cache_number
        })
        server.listen(+port || 3000)
        console.log(`监听地址：${port}`)
    })
program.parse(process.argv)