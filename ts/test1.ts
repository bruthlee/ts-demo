interface User {
    /*用户编码*/
    id: Number
    /*用户姓名*/
    name: String
}

export function showUser(user:User) {
    console.log(`编号：${user.id}, 姓名：${user.name}`)
}

const user: User = {
    id: 100,
    name: '张三'
}

showUser(user);