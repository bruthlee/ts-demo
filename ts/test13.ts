// 声明

/**
 * 声明变量
*/
declare var val1: string;
declare let val2: number;
declare const val3: boolean;
val1 = '1';
val1 = '2';
val2 = 1;
// val2 = '2'; 
// TS2322: Type 'string' is not assignable to type 'number'.
// val3 = true; 
// TS2588: Cannot assign to 'val3' because it is a constant.

/**
 * 声明函数
*/
declare function toString(x: number): string;
const x = toString(1); // => string
console.log('x: ', x)

// TS1183: An implementation cannot be declared in ambient contexts.
// 不能在环境上下文中声明实现。ts(1183)
// declare function toString(x: number) {
//     return String(x);  
// }

/**
 * 声明类
*/
declare class DecPerson {
    public name: string;
    private age: number;
    constructor(name: string);
    getAge(): number;  
}
const sub_person = new DecPerson('Mike');
console.log('name: ', sub_person.name);
// 属性“age”为私有属性，只能在类“Person”中访问
// sub_person.age;
sub_person.getAge();

/**
 * 声明枚举
*/
declare enum Direction {
    Up,
    Down,
    Left,
    Right,  
}  
const directions = [Direction.Up, Direction.Down, Direction.Left, Direction.Right];  

/**
 * 声明模块
*/
declare module 'testModule' {
    export function test<T>(): T
}

/**
 * 声明文件
*/
declare module '*.jpg' {
    const src: string
    export default src
}

/**
 * 声明命名空间
*/
declare namespace $ {
    const version: number
    function ajax(settings?: any): void
}
$.version

