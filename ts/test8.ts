/**
 * 联合和交叉类型
 */

type DAY = 'MONDAY' | 'TUESDAY' | 'WEDNESDAY' | 'THURSDAY' | 'FRIDAY' | 'SATURDAY' | 'SUNDAY'
let day: DAY
day = 'MONDAY'
day = 'MONDAY'

type OTHER_DAY = string & DAY
let other_day: OTHER_DAY
other_day = 'FRIDAY'

type ANOTHER_DAY = number & DAY // as never
let another_day: ANOTHER_DAY
// another_day = a 不能将类型“any”分配给类型“never”。ts(2322)

type A = string | number
type B = string | boolean

let a_b: A | B // string | number | boolean
a_b = 10
a_b = 'ab'
a_b = true

let ab: A & B // string
// ab = 10 不能将类型“number”分配给类型“string”。ts(2322)
ab = 'test'