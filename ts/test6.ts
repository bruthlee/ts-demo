class Animal {
    private type = 'Animal'
    protected sex = 'Male'
    public name = 'Hello'

    constructor(type: string, sex: string, name: string) {
        this.type = type
        this.sex = sex
        this.name = name
    }

    print() {
        console.log('my name is ', this.name, ', sex is ', this.sex)
    }

    bark(msg: string) {
        console.log(msg)
    }
}

class Dog extends Animal {
    constructor(sex: string, name: string) {
        super('Dog', sex, name);
        this.name = name + ' -- dog'
        this.sex = sex + ' -- ' + (sex==='Male'?'男狗':'女狗')
        // 属性“type”为私有属性，只能在类“Animal”中访问
        // this.type = 'abc'
    }

    bark() {
        super.bark('Woof! Woof! Woof!')
    }
}

const dog = new Dog('Male', 'Smart Belly')
dog.print()
dog.bark()