/**
 * 枚举类型
 */

enum ENUM_DAY {
    MONDAY,// 0
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY,
}

const day1 = ENUM_DAY.FRIDAY
console.log('Today is ', day1) // 4

enum DAY {
    MONDAY = 'MONDAY',// 0
    TUESDAY = 'TUESDAY',
    WEDNESDAY = 'WEDNESDAY',
    THURSDAY = 'THURSDAY',
    FRIDAY = 'FRIDAY',
    SATURDAY = 'SATURDAY',
    SUNDAY = 'SUNDAY',
}
const day2 = DAY.FRIDAY
console.log('Today is ', day2) // FRIDAY