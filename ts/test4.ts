// function fn1(): undefined {
//     console.log('fn1')
// }

function fn1(): void {
    console.log('fn1')
}

fn1()

type Addr = (a: number, b: number) => number;
const add: Addr = (a, b) => a + b;
console.log('add: ', add(1,2))


interface Entity {
    add: (a: number, b: number) => number;
    del(a: number, b: number): number;
}

const entity: Entity = {
    add: (a, b) => a + b,
    del(a, b) {
        return a - b;
    }
}
console.log('entity add: ', entity.add(1, 2));
console.log('entity del: ', entity.del(10, 2));


function testLog(msg?: string) {
    console.log('msg: ', msg)
    return msg
}
testLog()
testLog('test log')


function otherLog(msg: string | undefined) {
    console.log('otherLog msg: ', msg)
    return msg
}
// 未提供 "msg" 的自变量。
// otherLog()
otherLog('123')
otherLog(undefined)

console.log('=======================')
function log3(msg: number | string = 'test') {
    console.log('log3 msg: ', msg)
}
log3()
log3(1)
log3('abc')

console.log('=======================')
function myAddReduce(total: number, current: number, index: number, arr: number[]) {
    console.log(total, current, index, arr)
    return total + current
}
const arr = [1, 2, 3]
let res = arr.reduce(myAddReduce, 0)
console.log('res: ', res)

console.log('=======================')
function myMultiReduce(total: number, current: number, index: number, arr: number[]) {
    console.log(total, current, index, arr)
    return total * current
}
res = arr.reduce(myMultiReduce, 2)
console.log('res: ', res)

console.log('=======================')
function myExponentReduce(total: number, current: number, index: number, arr: number[]) {
    console.log(total, current, index, arr)
    return total ** current
}
res = arr.reduce(myExponentReduce)
console.log('res: ', res)
res = arr.reduce(myExponentReduce, 2)
console.log('res: ', res)

function sum(...numbers: number[]) {
    return numbers.reduce(myAddReduce)
}
console.log('sum: ', sum(1,2,3,4,5,6,7,8,9))