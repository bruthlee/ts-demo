let number: number[] = [1,2,3]
console.log(number)

let string: string[] = ['1', 'a', '1a']
console.log(string)

const str: string = 'string';
if (typeof str === 'string') {
  str.toLowerCase(); // Property 'toLowerCase' does not exist on type 'never'.ts(2339)
}
console.log('str: ', str);

let mayNullOrUndefinedOrString: null | undefined | string;
// mayNullOrUndefinedOrString!.toString(); // ok
// console.log(mayNullOrUndefinedOrString!.toString())
mayNullOrUndefinedOrString = 'abc';
console.log(mayNullOrUndefinedOrString!.toString())
// mayNullOrUndefinedOrString.toString(); // ts(2531)
