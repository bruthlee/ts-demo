/**
 * 二分查找
 */
function fun1(arr: number[], left: number, right: number, target: number): number {
    if (right >= left) {
        // 取中间数 left起步
        const mid = parseInt((left + (right - left)/2) + '')
        console.log(left, mid, right, target, ' => ', arr[mid])
        if (target == arr[mid]) {
            return mid
        }
        if (target < arr[mid]) {
            // 在左侧
            return fun1(arr, left, mid - 1, target)
        }
        else {
            // 在右侧
            return fun1(arr, mid + 1, right, target)
        }
    }
    return -1
}
let start = new Date().getTime()
let res = fun1([10,20,30,40,50,60,70,80,90], 0, 9, 80)
let end = new Date().getTime()
console.log(`${res} 耗时：${end-start}ms`) // 8ms