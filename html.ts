const list = document.getElementById('todo') as HTMLUListElement | null
const addButton = document.querySelector<HTMLButtonElement>('#add')
addButton?.addEventListener('click', btn_add)

function btn_remove(this: HTMLButtonElement, id: number) {
    const todo = this.parentElement
    todo && list?.removeChild(todo)
}

function btn_add() {
    const id = new Date().getTime()
    const el = document.createElement('li')
    el.innerHTML = `待办 ${id} <button>删除</button>`
    const button = el.getElementsByTagName('button')[0]
    button.style.color = 'red'
    if (button) {
        button.onclick = btn_remove.bind(button, id)
    }
    list?.appendChild(el)
}
